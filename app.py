# import the library
from appJar import gui

# handle button events
#def press(button):
    #if button == "Cancel":
    #    app.stop()
    #else:
        #usr = app.getEntry("Username")
        #pwd = app.getEntry("Password")
    #    print("User:", usr, "Pass:", pwd)

# create a GUI variable called app
app = gui("Chat", "400x600")
app.setSticky("swe")
app.setBg("red")
app.setInPadding([40,10]) # 20 pixels padding inside the widget [X, Y]
#app.setExpand("both")
app.setFont(14)

row = app.getRow()

app.addMessage("mess", """You can put a lot of text in this widget.
The text will be wrapped over multiple lines.
It's not possible to apply different styles to different words.
You can put a lot of text in this widget.
The text will be wrapped over multiple lines.
It's not possible to apply different styles to different words.
You can put a lot of text in this widget.
The text will be wrapped over multiple lines.
It's not possible to apply different styles to different words.""",colspan=2)
app.setMessageBg("mess", "orange")
app.setMessageAlign("mess", "left")
app.setMessageHeights("mess", 400)
app.setMessageInPadding("mess", [100, 200])
app.setInPadding([40,10])

# add & configure widgets - widgets get a name, to help referencing them later
#app.addLabel("title", "Welcome to appJar")
#app.setLabelBg("title", "blue")
#app.setLabelFg("title", "orange")

#app.addLabelEntry("Username")
def sendMsg(btn):
    msg = app.getEntry("Chat")
    print msg
    #mc.postToChat(msg)


app.addTextArea("t1", 1, 0, 2)
app.setTextAreaInPadding("t1", [100, 200])

#app.setFocus("Username")

# start the GUI
app.go()
