#-*- coding: utf-8 -*-
import os, signal, time
import socket, select, string, sys
import simplejson as json
import curses
import cPickle
import rsa
import readline

def prompt() :
    sys.stdout.write(":")
    sys.stdout.flush()

def start_function():
    if(len(sys.argv) < 4) :
        print 'Usage : python telnet.py hostname port nickname'
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])
    nickname = str(sys.argv[3])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    RECV_BUFFER = 4096

    # connect to remote host
    try :
        s.connect((host, port))
        s.send(nickname)
        key_data_pub = s.recv(RECV_BUFFER)
        key_data_priv = s.recv(RECV_BUFFER)
    except :
        print 'Unable to connect'
        sys.exit()

    print 'Connected to remote host. Start sending messages'
    os.system('cls' if os.name=='nt' else 'clear')


def main_script():

    prompt()

    while 1:

        signal.signal(signal.SIGINT, handler)

        socket_list = [sys.stdin, s]
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])

        for sock in read_sockets:
            if sock == s:
                data = sock.recv(RECV_BUFFER)
                data = rsa.decrypt(data, cPickle.loads(key_data_priv))
                if not data :
                    print '\nDisconnected from chat server'
                    sys.exit()
                else :
                    sys.stdout.write(data)
                    prompt()

            else :
                msg = nickname + " > " + sys.stdin.readline()
                if msg == nickname + " > " + "\n":
                    print "Please enter a message"
                    prompt()

                elif len(msg) > 230:
                    print "\nMessage is too long"
                    prompt()

                elif msg == nickname + " > " + "command exit" + "\n":
                    sys.exit(0)

                else:
                    encrypted_msg = rsa.encrypt(msg, cPickle.loads(key_data_pub))
                    s.send(encrypted_msg)
                    prompt()


if __name__ == "__main__":

    if(len(sys.argv) < 4) :
        print 'Usage : python telnet.py hostname port nickname'
        sys.exit()

    host = sys.argv[1]
    port = int(sys.argv[2])
    nickname = str(sys.argv[3])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    RECV_BUFFER = 4096

    # connect to remote host
    try :
        s.connect((host, port))
        s.send(nickname)
        key_data_pub = s.recv(RECV_BUFFER)
        key_data_priv = s.recv(RECV_BUFFER)
    except :
        print 'Unable to connect'
        sys.exit()

    print 'Connected to remote host. Start sending messages'
    os.system('cls' if os.name=='nt' else 'clear')

    def handler(signum, frame):
        print "For exit please type 'command exit'"
        time.sleep(2)
        main_script()

    main_script()
