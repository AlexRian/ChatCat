#-*- coding: utf-8 -*-
import socket, select
import rsa
import cPickle
import simplejson as json

(serv_pub, serv_priv) = rsa.newkeys(1024)

def broadcast_data (sock, message):
    for socket in CONNECTION_LIST:
        if socket != server_socket and socket != sock :
            try :
                socket.send(message)
            except :
                socket.close()
                CONNECTION_LIST.remove(socket)



if __name__ == "__main__":

    CONNECTION_LIST = []
    print CONNECTION_LIST
    RECV_BUFFER = 4096
    PORT = 6000

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)

    CONNECTION_LIST.append(server_socket)

    print "Chat server started on port " + str(PORT)

    while 1:

        read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST,[],[])

        for sock in read_sockets:
            if sock == server_socket:
                sockfd, addr = server_socket.accept()
                CONNECTION_LIST.append(sockfd)
                pre_data = sockfd.recv(RECV_BUFFER)
                client_addr = sockfd.getpeername()
                sockfd.sendto(cPickle.dumps(serv_pub), client_addr)
                sockfd.sendto(cPickle.dumps(serv_priv), client_addr)
                print "Client connected" + " " + pre_data + str(client_addr)
                broadcast_data(sockfd, rsa.encrypt("%s entered room\n" % pre_data, serv_pub))

            else:
                try:
                    data = sock.recv(RECV_BUFFER)
                    if data:
                        print CONNECTION_LIST
                        broadcast_data(sock, data)

                except:
                    broadcast_data(sock, rsa.encrypt("Client (%s) is offline\n" % pre_data, serv_pub))
                    #print CONNECTION_LIST
                    print "Client (%s) is offline" % pre_data
                    sock.close()
                    CONNECTION_LIST.remove(sock)
                    print CONNECTION_LIST
                    continue

    server_socket.close()
